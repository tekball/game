﻿namespace TekkenBall.View
{
    using Controller.States;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using View.UI;

    public class UIFactory
    {
        private object buttonNormal;

        public UIFactory()
        {
            this.Background = this.CreateSprite("background");
            this.MenuBackground = this.CreateSprite("menubackground");
            this.IntroBackground = this.CreateSprite("introbackground");
            this.NoPoint11 = this.CreateSprite("nopoint");
            this.NoPoint12 = this.CreateSprite("nopoint");
            this.NoPoint13 = this.CreateSprite("nopoint");
            this.NoPoint21 = this.CreateSprite("nopoint");
            this.NoPoint22 = this.CreateSprite("nopoint");
            this.NoPoint23 = this.CreateSprite("nopoint");
            this.GameOver = this.CreateSprite("gameoverscreen");
            this.StartButton = this.CreateButton("Start free", "Start hover", new Vector2((GloballyUsed.Graphics.PreferredBackBufferWidth - 255) / 2, 75), false);
            this.QuitButton = this.CreateButton("Quit free", "Quit hover", new Vector2((GloballyUsed.Graphics.PreferredBackBufferWidth - 255) / 2, 225), false);
        }

        public Sprite Background { get; set; }

        public Sprite MenuBackground { get; internal set; }

        public Sprite GameOver { get; internal set; }

        public Sprite IntroBackground { get; internal set; }

        public Sprite NoPoint11 { get; internal set; }

        public Sprite NoPoint12 { get; internal set; }

        public Sprite NoPoint13 { get; internal set; }

        public Sprite NoPoint21 { get; internal set; }

        public Sprite NoPoint22 { get; internal set; }

        public Sprite NoPoint23 { get; internal set; }

        public MenuButton StartButton { get; internal set; }

        public MenuButton QuitButton { get; internal set; }

        public Sprite CreateSprite(string fileName)
        {
            var texture = GloballyUsed.Content.Load<Texture2D>(fileName);
            Sprite sprite = new Sprite(texture);
            return sprite;
        }

        private MenuButton CreateButton(string buttonFree, string buttonHover, Vector2 position, bool isFree)
        {
            Texture2D free = GloballyUsed.Content.Load<Texture2D>(buttonFree);
            Texture2D hover = GloballyUsed.Content.Load<Texture2D>(buttonHover);
            Sprite sprite = new Sprite(free, position);
            MenuButton button = new MenuButton(sprite, free, hover, isFree);
            return button;
        }
    }
}