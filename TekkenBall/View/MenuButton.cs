﻿namespace TekkenBall.View
{
    using Microsoft.Xna.Framework.Graphics;
    using UI;

    public class MenuButton
    {
        public MenuButton(Sprite sprite, Texture2D free, Texture2D hover, bool isFree)
        {
            this.Sprite = sprite;
            this.Free = free;
            this.Hover = hover;
            this.IsFree = isFree;
        }

        public Sprite Sprite { get; set; }

        public Texture2D Free { get; set; }

        public Texture2D Hover { get; set; }

        public bool IsFree { get; set; }

        public void SwitchImage()
        {
            if (this.IsFree == true)
            {
                this.Sprite.Texture = this.Free;
            }
            else
            {
                this.Sprite.Texture = this.Hover;
            }
        }
    }
}