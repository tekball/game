﻿namespace TekkenBall.View
{
    using System.Collections.Generic;
    using Controller.States;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public class Renderer
    {
        public void DrawState(List<IRenderable> spritesToDraw)
        {
            GloballyUsed.SpriteBatch.Begin();
            foreach (IRenderable sprite in spritesToDraw)
            {
                if (sprite.IsFacingRight)
                {
                    // Scale is required therefore the long constructor
                    GloballyUsed.SpriteBatch.Draw(sprite.Texture, sprite.Position, sprite.SourceRectangle, Color.White, 0.0f, Vector2.Zero, sprite.Scale, SpriteEffects.None, 0);
                }
                else
                {
                    GloballyUsed.SpriteBatch.Draw(sprite.Texture, sprite.Position, sprite.SourceRectangle, Color.White, 0.0f, Vector2.Zero, sprite.Scale, SpriteEffects.FlipHorizontally, 0);
                }
            }
            
            GloballyUsed.SpriteBatch.End();
        }
    }
}