﻿namespace TekkenBall
{
    using System;
#if WINDOWS || LINUX
    public static class EntryPoint
    {
        [STAThread]
        public static void Main()
        {
            using (var game = new TekkenBall())
            {
                game.Run();
            }
        }
    }
#endif
}
