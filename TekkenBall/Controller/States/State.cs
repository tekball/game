﻿namespace TekkenBall.Controller.States
{
    using System.Collections.Generic;
    using Controller.Utilities;
    using View;

    public abstract class State
    {
        private InputHandler inputHandler;

        private UIFactory uiFactory;

        private bool stateDone;

        public State(InputHandler inputHandler, UIFactory uiFactory)
        {
            this.inputHandler = inputHandler;
            this.UiFactory = uiFactory;
            this.NextState = this;
            this.SpritesInState = new List<IRenderable>();
            this.StateDone = false;
        }

        public State NextState { get; set; }

        public List<IRenderable> SpritesInState { get; set; }

        public bool StateDone
        {
            get
            {
                return this.stateDone;
            }

            set
            {
                this.stateDone = value;
            }
        }

        public UIFactory UiFactory
        {
            get
            {
                return this.uiFactory;
            }

            set
            {
                this.uiFactory = value;
            }
        }

        public InputHandler InputHandler
        {
            get
            {
                return this.inputHandler;
            }

            set
            {
                this.inputHandler = value;
            }
        }

        public virtual void Draw(Renderer renderer)
        {
            renderer.DrawState(this.SpritesInState);
        }

        public virtual void Update()
        {
            if (!this.StateDone)
            {
                this.InputHandler.Update();
            }
        }
    }
}