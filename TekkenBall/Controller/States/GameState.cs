﻿namespace TekkenBall.Controller.States
{
    using System;
    using System.Collections.Generic;
    using Controller.Utilities;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Model.Player;
    using View;
    using View.UI;

    public class GameState : State
    {
        private SpriteFont font;

        // for future use; players can type names
        private string leftNinjaName;
        private string rightNinjaName;

        private List<Sprite> kunai;

        private List<Player> players;
        private List<Animation> player1Animations;
        private List<Animation> player2Animations;

        public GameState(InputHandler inputHandler, UIFactory uiFactory, List<Player> ninjas = null)
            : base(inputHandler, uiFactory)
        {
            this.SpritesInState.Add(this.UiFactory.Background);
            this.font = GloballyUsed.Content.Load<SpriteFont>("Font");
            this.player1Animations = new List<Animation>();
            this.player2Animations = new List<Animation>();

            if (this.leftNinjaName == null)
            {
                this.leftNinjaName = "Ninja 1";
            }

            if (this.rightNinjaName == null)
            {
                this.rightNinjaName = "Ninja 2";
            }

            if (ninjas == null)
            {
                Player leftNinja = new Player(Keys.A, Keys.D, Keys.W, Keys.F, Keys.G, new Vector2(100, 200), true);
                Player rightNinja = new Player(Keys.Left, Keys.Right, Keys.Up, Keys.N, Keys.M, new Vector2(400, 200), false);

                this.players = new List<Player>();
                this.players.Add(leftNinja);
                this.players.Add(rightNinja);
            }
            else
            {
                this.players = ninjas;
            }

            this.kunai = new List<Sprite>();

            this.Initialize();
        }

        public string LeftNinjaName
        {
            get
            {
                return this.leftNinjaName;
            }

            set
            {
                this.leftNinjaName = value;
            }
        }

        public string RightNinjaName
        {
            get
            {
                return this.rightNinjaName;
            }

            set
            {
                this.rightNinjaName = value;
            }
        }

        public void Initialize()
        {
            this.Player1LoadAnimations();
            this.Playe2LoadAnimations();

            // Player 1 score
            this.UiFactory.NoPoint11.Position = new Vector2(10, 45);
            this.SpritesInState.Add(this.UiFactory.NoPoint11);
            this.UiFactory.NoPoint12.Position = new Vector2(40, 45);
            this.SpritesInState.Add(this.UiFactory.NoPoint12);
            this.UiFactory.NoPoint13.Position = new Vector2(70, 45);
            this.SpritesInState.Add(this.UiFactory.NoPoint13);

            // Player 2 score
            this.UiFactory.NoPoint21.Position = new Vector2(690, 45);
            this.SpritesInState.Add(this.UiFactory.NoPoint21);
            this.UiFactory.NoPoint22.Position = new Vector2(720, 45);
            this.SpritesInState.Add(this.UiFactory.NoPoint22);
            this.UiFactory.NoPoint23.Position = new Vector2(750, 45);
            this.SpritesInState.Add(this.UiFactory.NoPoint23);
        }

        public override void Update()
        {
            base.Update();

            if (!this.StateDone)
            {
                this.CheckForPause();

                this.CheckGameOver();
                for (int i = 0; i < this.players.Count; i++)
                {
                    // this.UpdateUI(i);
                    this.UpdatePlayer(i);

                    // this.PlayerShoot(i);
                }
            }
        }

        public override void Draw(Renderer renderer)
        {
            base.Draw(renderer);
            GloballyUsed.SpriteBatch.Begin();
            for (int i = 0; i < this.player1Animations.Count; i++)
            {
                this.player1Animations[i].Draw(GloballyUsed.SpriteBatch, player1Animations[i].Position, new Vector2(0.2f, 0.2f));
            }
            GloballyUsed.SpriteBatch.DrawString(this.font, this.leftNinjaName, new Vector2(40, 15), Color.DarkRed);
            GloballyUsed.SpriteBatch.DrawString(this.font, this.rightNinjaName, new Vector2(720, 15), Color.DarkRed);
            GloballyUsed.SpriteBatch.End();
        }

        private void Player1LoadAnimations()
        {
            Animation player1Animation1 = new Animation(GloballyUsed.Content.Load<Texture2D>("AttackAnimation1"), 10, 4, 3, 536, 495);
            GloballyUsed.Content.Load<Texture2D>("AttackAnimation1");
            this.player1Animations.Add(player1Animation1);
            Animation player1Animation2 = new Animation(GloballyUsed.Content.Load<Texture2D>("ClimbAnimation1"), 10, 3, 4, 282, 464);
            GloballyUsed.Content.Load<Texture2D>("ClimbAnimation1");
            this.player1Animations.Add(player1Animation2);
            Animation player1Animation3 = new Animation(GloballyUsed.Content.Load<Texture2D>("DeadAnimation1"), 10, 3, 4, 482, 498);
            GloballyUsed.Content.Load<Texture2D>("DeadAnimation1");
            this.player1Animations.Add(player1Animation3);
            Animation player1Animation4 = new Animation(GloballyUsed.Content.Load<Texture2D>("GlideAnimation1"), 10, 3, 4, 443, 454);
            GloballyUsed.Content.Load<Texture2D>("GlideAnimation1");
            this.player1Animations.Add(player1Animation4);
            Animation player1Animation5 = new Animation(GloballyUsed.Content.Load<Texture2D>("IdleAnimation1"), 10, 2, 5, 232, 439);
            GloballyUsed.Content.Load<Texture2D>("IdleAnimation1");
            this.player1Animations.Add(player1Animation5);
            Animation player1Animation6 = new Animation(GloballyUsed.Content.Load<Texture2D>("JumpAnimation1"), 10, 3, 4, 362, 483);
            GloballyUsed.Content.Load<Texture2D>("JumpAnimation1");
            this.player1Animations.Add(player1Animation6);
            Animation player1Animation7 = new Animation(GloballyUsed.Content.Load<Texture2D>("JumpAttack1"), 10, 3, 4, 504, 522);
            GloballyUsed.Content.Load<Texture2D>("JumpAttack1");
            this.player1Animations.Add(player1Animation7);
            Animation player1Animation8 = new Animation(GloballyUsed.Content.Load<Texture2D>("JumpThrow1"), 10, 3, 4, 360, 431);
            GloballyUsed.Content.Load<Texture2D>("JumpThrow1");
            this.player1Animations.Add(player1Animation8);
            Animation player1Animation9 = new Animation(GloballyUsed.Content.Load<Texture2D>("RunAnimation1"), 10, 3, 4, 363, 458);
            GloballyUsed.Content.Load<Texture2D>("RunAnimation1");
            this.player1Animations.Add(player1Animation9);
            Animation player1Animation10 = new Animation(GloballyUsed.Content.Load<Texture2D>("SlideAnimation1"), 10, 4, 3, 373, 351);
            GloballyUsed.Content.Load<Texture2D>("SlideAnimation1");
            this.player1Animations.Add(player1Animation10);
            Animation player1Animation11 = new Animation(GloballyUsed.Content.Load<Texture2D>("ThrowAnimation1"), 10, 3, 4, 377, 451);
            GloballyUsed.Content.Load<Texture2D>("ThrowAnimation1");
            this.player1Animations.Add(player1Animation11);
        }

        private void Playe2LoadAnimations()
        {
            Animation player2Animation1 = new Animation(GloballyUsed.Content.Load<Texture2D>("AttackAnimation2"), 10, 3, 4, 524, 565);
            GloballyUsed.Content.Load<Texture2D>("AttackAnimation2");
            this.player2Animations.Add(player2Animation1);
            Animation player2Animation2 = new Animation(GloballyUsed.Content.Load<Texture2D>("ClimbAnimation2"), 10, 3, 4, 361, 497);
            GloballyUsed.Content.Load<Texture2D>("ClimbAnimation2");
            this.player2Animations.Add(player2Animation2);
            Animation player2Animation3 = new Animation(GloballyUsed.Content.Load<Texture2D>("DeadAnimation2"), 10, 3, 4, 578, 599);
            GloballyUsed.Content.Load<Texture2D>("DeadAnimation2");
            this.player2Animations.Add(player2Animation3);
            Animation player2Animation4 = new Animation(GloballyUsed.Content.Load<Texture2D>("GlideAnimation2"), 10, 4, 3, 505, 474);
            GloballyUsed.Content.Load<Texture2D>("GlideAnimation2");
            this.player2Animations.Add(player2Animation4);
            Animation player2Animation5 = new Animation(GloballyUsed.Content.Load<Texture2D>("IdleAnimation2"), 10, 2, 5, 290, 500);
            GloballyUsed.Content.Load<Texture2D>("IdleAnimation2");
            this.player2Animations.Add(player2Animation5);
            Animation player2Animation6 = new Animation(GloballyUsed.Content.Load<Texture2D>("JumpAnimation2"), 10, 3, 4, 399, 543);
            GloballyUsed.Content.Load<Texture2D>("JumpAnimation2");
            this.player2Animations.Add(player2Animation6);
            Animation player2Animation7 = new Animation(GloballyUsed.Content.Load<Texture2D>("JumpAttack2"), 10, 3, 4, 495, 583);
            GloballyUsed.Content.Load<Texture2D>("JumpAttack2");
            this.player2Animations.Add(player2Animation7);
            Animation player2Animation8 = new Animation(GloballyUsed.Content.Load<Texture2D>("JumpThrow2"), 10, 3, 4, 425, 495);
            GloballyUsed.Content.Load<Texture2D>("JumpThrow2");
            this.player2Animations.Add(player2Animation8);
            Animation player2Animation9 = new Animation(GloballyUsed.Content.Load<Texture2D>("RunAnimation2"), 10, 3, 4, 376, 520);
            GloballyUsed.Content.Load<Texture2D>("RunAnimation2");
            this.player2Animations.Add(player2Animation9);
            Animation player2Animation10 = new Animation(GloballyUsed.Content.Load<Texture2D>("SlideAnimation2"), 10, 3, 4, 397, 401);
            GloballyUsed.Content.Load<Texture2D>("SlideAnimation2");
            this.player2Animations.Add(player2Animation10);
            Animation player2Animation11 = new Animation(GloballyUsed.Content.Load<Texture2D>("ThrowAnimation2"), 10, 3, 4, 383, 514);
            GloballyUsed.Content.Load<Texture2D>("ThrowAnimation2");
            this.player2Animations.Add(player2Animation11);
        }

        private void UpdatePlayer(int i)
        {
            this.players[i].HandleMovement(this.InputHandler.ActiveKeys);
            if (i == 0)
            {
                for (int j = 0; j < this.player1Animations.Count; j++)
                {
                    this.player1Animations[j].Position = this.players[i].Position;
                    this.player1Animations[j].IsFacingRight = this.players[i].IsFacingRight;
                }
            }
            else if (i == 1)
            {
                for (int j = 0; j < this.player2Animations.Count; j++)
                {
                    this.player1Animations[j].Position = this.players[i].Position;
                    this.player1Animations[j].IsFacingRight = this.players[i].IsFacingRight;
                }
            }
        }

        private void CheckForPause()
        {
            foreach (var button in this.InputHandler.ActiveKeys)
            {
                if (button.Key == Keys.P && button.KeyState == KeyboardKeyState.Pushed)
                {
                    this.StateDone = true;
                    this.NextState = new Pause(this.InputHandler, this.UiFactory, this.players);
                }
            }
        }

        private void CheckGameOver()
        {
            for (int i = 0; i < this.players.Count; i++)
            {
                if (this.players[i].Score == 3)
                {
                    this.StateDone = true;
                    this.NextState = new GameOver(this.InputHandler, this.UiFactory, i);
                }
            }
        }
    }
}