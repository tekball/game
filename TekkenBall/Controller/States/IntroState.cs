﻿namespace TekkenBall.Controller.States
{
    using Controller.Utilities;
    using Microsoft.Xna.Framework.Input;
    using View;

    public class IntroState : State
    {
        public IntroState(InputHandler inputHandler, UIFactory uiFactory)
            : base(inputHandler, uiFactory)
        {
            this.SpritesInState.Add(this.UiFactory.IntroBackground);
        }

        public override void Update()
        {
            base.Update();
            
            foreach (KeyboardButtonState button in this.InputHandler.ActiveKeys)
            {
                if (button.Key == Keys.Space && button.KeyState == KeyboardKeyState.Pushed)
                {
                    this.StateDone = true;
                    this.NextState = new MenuState(this.InputHandler, this.UiFactory);
                }
            }
        }
    }
}
