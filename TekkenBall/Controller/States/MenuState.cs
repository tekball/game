﻿namespace TekkenBall.Controller.States
{
    using System.Text;
    using Controller.Utilities;
    using Microsoft.Xna.Framework.Input;
    using View;

    public delegate void OnGameQuit();

    public class MenuState : State
    {
        private byte menuOption;

        public MenuState(InputHandler inputHandler, UIFactory uiFactory)
            : base(inputHandler, uiFactory)
        {
            this.SpritesInState.Add(this.UiFactory.MenuBackground);
            this.SpritesInState.Add(this.UiFactory.StartButton.Sprite);
            this.SpritesInState.Add(this.UiFactory.QuitButton.Sprite);
            this.menuOption = 1;
        }

        public static event OnGameQuit OnExitPressed;

        public override void Update()
        {
            base.Update();

            if (!this.StateDone)
            {
                foreach (KeyboardButtonState button in this.InputHandler.ActiveKeys)
                {
                    if (button.Key == Keys.Up && button.KeyState == KeyboardKeyState.Pushed)
                    {
                        --this.menuOption;

                        if (this.menuOption < 1)
                        {
                            this.menuOption = 2;
                        }
                    }
                    else if (button.Key == Keys.Down && button.KeyState == KeyboardKeyState.Pushed)
                    {
                        ++this.menuOption;

                        if (this.menuOption > 2)
                        {
                            this.menuOption = 1;
                        }
                    }

                    if (button.Key == Keys.Enter && button.KeyState == KeyboardKeyState.Pushed)
                    {
                        switch (this.menuOption)
                        {
                            case 1:
                                this.StartGame();
                                break;
                            case 2:
                                this.QuitGame();
                                break;
                        }
                    }
                }
            }

            this.ChangeButtonsState();
        }
        
        private void StartGame()
        {
            this.StateDone = true;
            this.NextState = new GameState(this.InputHandler, this.UiFactory);
        }

        private void QuitGame()
        {
            this.StateDone = true;
            MenuState.OnExitPressed.Invoke();
        }

        private void ChangeButtonsState()
        {
            this.UiFactory.StartButton.Sprite.Texture = this.UiFactory.StartButton.Free;
            this.UiFactory.QuitButton.Sprite.Texture = this.UiFactory.QuitButton.Free;

            switch (this.menuOption)
            {
                case 1:
                    this.UiFactory.StartButton.SwitchImage();
                    break;
                case 2:
                    this.UiFactory.QuitButton.SwitchImage();
                    break;
            }
        }
    }
}