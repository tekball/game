﻿namespace TekkenBall.Controller.States
{
    using System.Collections.Generic;
    using Controller.Utilities;
    using Model.Player;
    using View;
    using Microsoft.Xna.Framework.Input;

    public class Pause : State
    {
        private List<Player> players;

        public Pause(InputHandler inputHandler, UIFactory uiFactory, List<Player> ninjas)
            : base(inputHandler, uiFactory)
        {
            this.players = ninjas;

            this.SpritesInState.Add(this.UiFactory.IntroBackground);

            this.MenuId = 1;
        }

        public int MenuId { get; private set; }

        public override void Update()
        {
            base.Update();

            if (!this.StateDone)
            {
                foreach (var button in this.InputHandler.ActiveKeys)
                {
                    if (button.Key == Keys.Space && button.KeyState == KeyboardKeyState.Pushed)
                    {
                        this.StateDone = true;
                        this.NextState = new GameState(this.InputHandler, this.UiFactory, this.players);
                    }
                }
            }
        }
    }
}