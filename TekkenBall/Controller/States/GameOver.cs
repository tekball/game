﻿namespace TekkenBall.Controller.States
{
    using Controller.Utilities;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using View;

    public class GameOver : State
    {
        private SpriteFont font;
        private int notLoser;

        public GameOver(InputHandler inputHandler, UIFactory uiFactory, int winner)
            : base(inputHandler, uiFactory)
        {
            this.notLoser = winner;
            this.font = GloballyUsed.Content.Load<SpriteFont>("Arial");
            this.SpritesInState.Add(this.UiFactory.GameOver);
        }

        public override void Update()
        {
            base.Update();

            if (!this.StateDone)
            {
                foreach (KeyboardButtonState button in this.InputHandler.ActiveKeys)
                {
                    if (button.Key == Keys.Space && button.KeyState == KeyboardKeyState.Pushed)
                    {
                        this.StateDone = true;
                        this.NextState = new MenuState(this.InputHandler, this.UiFactory);
                    }
                }
            }
        }

        public override void Draw(Renderer renderer)
        {
            base.Draw(renderer);
            GloballyUsed.SpriteBatch.Begin();
            GloballyUsed.SpriteBatch.DrawString(font, "Player " + (this.notLoser + 1) + " wins!", new Vector2(350, 300), Color.DarkBlue);
            GloballyUsed.SpriteBatch.End();
        }
    }
}
