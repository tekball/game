﻿namespace TekkenBall.Controller.States
{
    using System;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    public class GloballyUsed
    {
        private static Random rng;

        public static Random Rng
        {
            get
            {
                return rng;
            }

            set
            {
                rng = new Random();
            }
        }

        public static SpriteBatch SpriteBatch { get; set; }

        public static GraphicsDeviceManager Graphics { get; set; }

        public static GameTime GameTime { get; set; }

        public static ContentManager Content { get; set; }
    }
}
