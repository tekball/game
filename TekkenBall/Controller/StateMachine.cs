﻿namespace TekkenBall.Controller
{
    using Controller.States;
    using Controller.Utilities;
    using View;

    public class StateMachine
    {
        public StateMachine(InputHandler inputHandler, UIFactory uiFactory)
        {
            // First state
            this.CurrentState = new IntroState(inputHandler, uiFactory);
        }

        public State CurrentState { get; set; }

        public void Update()
        {
            this.CurrentState.Update();
            this.CurrentState = this.CurrentState.NextState;
        }

        public void Draw(Renderer renderer)
        {
            this.CurrentState.Draw(renderer);
        }
    }
}