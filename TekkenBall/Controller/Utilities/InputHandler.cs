﻿namespace TekkenBall.Controller.Utilities
{
    using System.Collections.Generic;
    using Microsoft.Xna.Framework.Input;

    public class InputHandler
    {
        public InputHandler()
        {
            this.ActiveKeys = new List<KeyboardButtonState>();
        }
        
        public KeyboardState CurrentKeyboardState { get; set; }

        public KeyboardState PreviousKeyboardState { get; set; }

        public List<KeyboardButtonState> ActiveKeys { get; set; }

        public Keys KeyToCheck { get; set; }

        public void Update()
        {
            this.PreviousKeyboardState = this.CurrentKeyboardState;
            this.CurrentKeyboardState = Keyboard.GetState();
            this.CheckKeys();
        }

        public void CheckKeys()
        {
            for (int i = 0; i < this.CurrentKeyboardState.GetPressedKeys().Length; i++)
            {
                this.KeyToCheck = this.CurrentKeyboardState.GetPressedKeys()[i];
                
                if (this.PreviousKeyboardState.IsKeyUp(this.KeyToCheck) &&
                    this.CurrentKeyboardState.IsKeyDown(this.KeyToCheck))
                {
                    this.ActiveKeys.Add(new KeyboardButtonState(this.KeyToCheck));
                }
                else if (this.PreviousKeyboardState.IsKeyDown(this.KeyToCheck) &&
                    this.CurrentKeyboardState.IsKeyDown(this.KeyToCheck))
                {
                    foreach (KeyboardButtonState key in this.ActiveKeys)
                    {
                        if (key.Key == this.KeyToCheck)
                        {
                            key.KeyState = KeyboardKeyState.HeldDown;
                        }
                    }
                }
            }

            for (int i = 0; i < this.ActiveKeys.Count; i++)
            {
                if (this.PreviousKeyboardState.IsKeyUp(this.ActiveKeys[i].Key) &&
                    this.CurrentKeyboardState.IsKeyUp(this.ActiveKeys[i].Key))
                {
                    this.ActiveKeys[i].Key = Keys.None;
                    this.ActiveKeys[i].KeyState = KeyboardKeyState.None;
                }
            }

            while (this.ActiveKeys.Contains(new KeyboardButtonState(Keys.None)))
            {
                this.ActiveKeys.Remove(new KeyboardButtonState(Keys.None));
            }
        }
    }
}