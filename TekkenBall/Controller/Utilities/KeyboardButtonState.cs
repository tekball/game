﻿namespace TekkenBall.Controller.Utilities
{
    using Microsoft.Xna.Framework.Input;

    public enum KeyboardKeyState
    {
        None,
        Pushed,
        HeldDown,
        Released
    }

    public class KeyboardButtonState
    {
        public KeyboardButtonState(Keys pushedKey)
        {
            this.Key = pushedKey;
            this.KeyState = KeyboardKeyState.Pushed;
        }

        public Keys Key { get; set; }

        public KeyboardKeyState KeyState { get; set; }
    }
}