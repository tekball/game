﻿namespace TekkenBall
{
    using Controller;
    using Controller.States;
    using Controller.Utilities;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using View;

    public class TekkenBall : Game
    {
        private Renderer renderer;

        private StateMachine stateMachine;

        private UIFactory uiFactory;
        private InputHandler inputHandler;
        
        public TekkenBall()
        {
            GloballyUsed.Graphics = new GraphicsDeviceManager(this);
            GloballyUsed.Content = this.Content;
            Content.RootDirectory = "Content";
            Window.Title = "TekkenBall";
            this.IsMouseVisible = false;

            GloballyUsed.Graphics.PreferredBackBufferWidth = 799;
            GloballyUsed.Graphics.PreferredBackBufferHeight = 335;

            MenuState.OnExitPressed += this.QuitGame;
        }

        protected override void Initialize()
        {
            this.uiFactory = new UIFactory();
            this.renderer = new Renderer();
            this.inputHandler = new InputHandler();
            this.stateMachine = new StateMachine(this.inputHandler, this.uiFactory);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            GloballyUsed.SpriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            GloballyUsed.GameTime = gameTime;
            this.stateMachine.Update();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            this.stateMachine.Draw(this.renderer);
        }

        private void QuitGame()
        {
            this.Exit();
        }
    }
}
