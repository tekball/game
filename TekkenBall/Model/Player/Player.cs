﻿namespace TekkenBall.Model.Player
{
    using System.Collections.Generic;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using global::TekkenBall.Controller.Utilities;

    public enum PlayerStates
    {
        ATTACKING,
        CLIMBING,
        DEAD,
        GLIDING,
        IDLE,
        JUMPING,
        JUMPATTACKING,
        JUMPTHROWING,
        RUNNING,
        SLIDING,
        SHOOTING
    }

    public class Player
    {
        private const int PLAYER_SPEED = 8;
        private const int JUMP_VELOCITY = 9;

        private const int LEFT_BOUND = 0;
        private const int RIGHT_BOUND = 1280;
        private const int KUNAI_AMMO = 1;
        private const int STARTING_HEALTH = 100;

        private Dictionary<string, Keys> controls;
        private Vector2 velocity;
        private SpriteBatch spriteBatch;
        private bool isMoving;
        private float jumpHeight;
        private bool isGrounded;
        private bool canAttack;
        private int attackTimer;

        public Player(Keys moveLeft, Keys moveRight, Keys jump, Keys attack, Keys shoot, Vector2 position, bool isFacingRight)
        {
            this.State = PlayerStates.IDLE;
            this.IsFacingRight = isFacingRight;
            this.jumpHeight = 0;

            this.controls = new Dictionary<string, Keys>();
            this.controls.Add("Move Left", moveLeft);
            this.controls.Add("Move Right", moveRight);
            this.controls.Add("Attack", attack);
            this.controls.Add("Jump", jump);
            this.controls.Add("Shoot", shoot);
            this.Score = 0;

            this.Position = position;
            this.Health = STARTING_HEALTH;
        }

        public byte Score { get; set; }

        public Vector2 Position { get; set; }

        public Rectangle Bounds { get; set; }

        public PlayerStates State { get; private set; }

        public bool IsFacingRight { get; set; }

        public bool IsShooting { get; set; }

        public int Health { get; set; }

        public int Kunai { get; set; }

        public void HandleMovement(List<KeyboardButtonState> activeKeys)
        {
            this.isMoving = false;

            foreach (var key in activeKeys)
            {
                if (key.Key == this.controls["Move Left"] && key.KeyState == KeyboardKeyState.HeldDown && !this.isMoving)
                {
                    this.State = PlayerStates.RUNNING;
                    this.isMoving = true;
                    this.MoveLeft();
                }
                else if (key.Key == this.controls["Move Right"] && key.KeyState == KeyboardKeyState.HeldDown && !this.isMoving)
                {
                    this.State = PlayerStates.RUNNING;
                    this.isMoving = true;
                    this.MoveRight();
                }
                else if (key.Key == this.controls["Jump"] && key.KeyState == KeyboardKeyState.HeldDown)
                {
                    this.Jump();
                }
                else if (key.Key == this.controls["Shoot"] && key.KeyState == KeyboardKeyState.Pushed)
                {
                    this.Shoot();
                }
            }
        }

        private void Shoot()
        {
            if (this.Kunai > 0)
            {
                this.IsShooting = true;
                this.Kunai--;
            }
        }

        private void Jump()
        {
            if (this.isGrounded)
            {
                this.isGrounded = false;
                this.velocity = new Vector2(this.velocity.X, -JUMP_VELOCITY);
            }
        }

        private void MoveLeft()
        {
            if (!Keyboard.GetState().IsKeyDown(this.controls["Move Right"]))
            {
                this.IsFacingRight = false;
            }
        }

        private void MoveRight()
        {
            if (!Keyboard.GetState().IsKeyDown(this.controls["Move Left"]))
            {
                this.IsFacingRight = true;
            }
        }
    }
}
