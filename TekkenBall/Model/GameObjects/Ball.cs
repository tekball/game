﻿namespace TekkenBall.Model.GameObjects
{
    using System;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public class Ball : GameObject
    {
        private Vector2 originalPosition;
        private Random rand = new Random();

        public Ball(Texture2D texture2D, Vector2 position, Rectangle screenBounds, Rectangle colision) :
                        base(texture2D, position, screenBounds)
        {
            this.originalPosition = position;
        }

        public override void ActOnCollision()
        {
            throw new NotImplementedException();
        }

        public void Reset()
        {
            this.Position = new Vector2(this.originalPosition.X, this.originalPosition.Y);
            this.SetDefaultSpeed();
            this.SetNextDirection();
        }
        
        public void ReverseYDirection()
        {
            this.IsFacingRight = !this.IsFacingRight;
        }

        public void SetPosition(float x, float y)
        {
            this.Position = new Vector2(x, y);
        }

        private void SetDefaultSpeed()
        {
            this.Velocity = new Vector2(0.35f, 0.35f);
        }

        private void SetNextDirection()
        {
            // calculate angle from jump attack
        }

        private void Bounce()
        {
            // bounce off walls physics
        }

        private void Update()
        {   
        }

        private void CheckBounds()
        {
            // check if it hits a wall
        }

        private void CheckPlayerCollision()
        {
            // check if ap layer is hit
        }
    }
}