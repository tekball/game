﻿namespace TekkenBall.Model.GameObjects
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public abstract class GameObject
    {
        private bool isFacingRight;

        public GameObject(Texture2D texture, Vector2 location, Rectangle screenBounds)
        {
            this.Texture = texture;
            this.Position = location;
            this.Velocity = Vector2.Zero;
            this.Bounds = screenBounds;
            this.IsFacingRight = true;
        }

        public Texture2D Texture { get; set; }

        public Vector2 Velocity { get; set; }

        public Vector2 Position { get; set; }

        public bool IsFacingRight
        {
            get
            {
                return this.isFacingRight;
            }

            set
            {
                this.isFacingRight = value;
            }
        }

        public Rectangle Colision { get; set; }

        public Rectangle Bounds { get; set; }

        public abstract void ActOnCollision();
    }
}
